require 'rails_helper'

RSpec.describe Hostel, type: :model do
  describe "#current_tasks" do
    before :all do
      @hostel = FactoryGirl.create(:hostel, :with_rooms)
      @user = FactoryGirl.create(:user, hostel: @hostel)
      @room = @hostel.rooms.first
      FactoryGirl.create_list(:current_task, 10, :with_tasks, hostel: @hostel, room: @room)

      @other_hostel = FactoryGirl.create(:hostel, :with_rooms)
      @other_room = @other_hostel.rooms.first
      FactoryGirl.create_list(:current_task, 5, :with_tasks, hostel: @other_hostel, room: @other_room)
    end

    it "actually returns current_tasks" do
      current_task = @hostel.current_tasks.first
      expect(current_task).to eq(CurrentTask.find(current_task.id))
    end

    it "returns all current_tasks only from the hostel of the current user" do
      expect(@hostel.current_tasks.count).to eq(10)
    end
  end
end

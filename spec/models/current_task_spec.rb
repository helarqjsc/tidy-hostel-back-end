require 'rails_helper'

RSpec.describe CurrentTask, type: :model do
  describe "#change_room_status" do
    before :all do
      @hostel = FactoryGirl.create(:hostel, :with_rooms)
      @user = FactoryGirl.create(:user, hostel: @hostel)
      @room = @hostel.rooms.first
      FactoryGirl.create_list(:current_task, 5, :with_tasks, hostel: @hostel, room: @room)

      @other_hostel = FactoryGirl.create(:hostel, :with_rooms)
      @other_room = @other_hostel.rooms.first
      FactoryGirl.create_list(:current_task, 5, :with_tasks, hostel: @other_hostel, room: @other_room)
    end


    it "is called on current_task save and changes room status" do
      expect(@room.status).to eq("dirty")
      @room.current_tasks.each do |ct|
        ct.done = true
        ct.save
      end
      expect(@room.status).to eq("clean")
    end
  end
end

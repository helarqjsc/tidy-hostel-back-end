module Helpers
  def login(user)
    logout
    session = UserSession.create(email: user.email, password: user.password)
    # byebug if session.user.nil?
    @token = session.user.single_access_token
  end

  def logout
    session = UserSession.find
    session.destroy if session
  end

  def json
    JSON.parse(response.body)
  end

  [:get, :post, :delete, :put].each do |action|
    define_method "#{action.to_s}_with_token" do |*args|
      params = args[1] || {}
      send(action, args[0], {token: @token}.merge(params))
    end
  end
end
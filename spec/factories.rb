FactoryGirl.define do

  factory :hostel do
    sequence(:name){ |n| Faker::Lorem.sentence + n.to_s }
    phone { Faker::PhoneNumber.phone_number }
    email { Faker::Internet.email }
    website { Faker::Internet.url }

    country { Faker::Address.country }
    city { Faker::Address.city }
    postal_code { Faker::Address.postcode }
    address_1 { Faker::Address.street_address }
    address_2 { Faker::Address.secondary_address }

    noun = Faker::Hacker.noun
    vkontakte { 'http://vk.com/' + noun }
    facebook { 'http://facebook.com/' + noun }
    twitter { 'http://twitter.com/' + noun }
    foursquare { 'http://foursquare.com/' + noun }

    trait :with_rooms do
      after :build do |hostel|
        create_list(:room, 5, hostel: hostel)
      end
    end
  end


  factory :user do
    email { Faker::Internet.email }
    password { Faker::Internet.password }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    phone { Faker::PhoneNumber.phone_number }
    group { 'owner' }
  end


  factory :room do
    sequence(:number)
    room_class { Room.room_classes.keys.sample }
    status { Room.statuses.keys.sample }
  end


  factory :task do
    name { Faker::Hacker.say_something_smart }
    enabled { true }
  end


  factory :current_task do
    transient do
      hostel true
    end

    done { false }

    trait :with_tasks do
      after :build do |current_task, evaluator|
        current_task.task = create(:task, hostel: evaluator.hostel)
      end
    end
  end

  factory :handyman_task do
    message { Faker::Hacker.say_something_smart }
    status { HandymanTask.statuses.keys.sample }
    priority { HandymanTask.priorities.keys.sample }
    photo { File.new("#{Rails.root}/spec/support/fixtures/image.jpg") }
    photo_data { Base64.encode64(photo.read) }
  end


end

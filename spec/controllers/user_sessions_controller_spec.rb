require 'rails_helper'

RSpec.describe UserSessionsController, type: :controller do
  before do
    @hostel = FactoryGirl.build(:hostel)
    @user = FactoryGirl.create(:user, hostel: @hostel)
  end

  describe "POST /auth/login" do
    it "returns success with correct credentials" do
      post :create, {email: @user.email,
                     password: @user.password}
      expect(response).to have_http_status(200)
    end

    it "returns unauthorized with incorrect credentials" do
      post :create, {email: 'whatevs',
                     password: 321}
      expect(response).to have_http_status(401)
    end
  end

  describe "POST /auth/logout" do
    it "successfuly destroys user session" do
      post :create, {email: @user.email,
                     password: @user.password}
      post :destroy
      expect(response).to have_http_status(200)
      expect(UserSession.find).to be_nil
    end

  end


end

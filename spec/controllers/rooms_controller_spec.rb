require 'rails_helper'

RSpec.describe RoomsController, type: :controller do
  include Helpers
  before do
    activate_authlogic
    @hostel_main = FactoryGirl.create(:hostel, :with_rooms)
    @hostel_second = FactoryGirl.create(:hostel)
    @correct_user = FactoryGirl.create(:user, hostel: @hostel_main)
    @incorrect_user = FactoryGirl.create(:user, hostel: @hostel_second)
  end

  context "" do
    cleaner = FactoryGirl.create(:user, hostel: FactoryGirl.create(:hostel), group: 'cleaner')
    it_behaves_like "unauthorized"
    it_behaves_like "unauthorized", user: cleaner, only: [:destroy, :create, :update]
  end

  describe "while authenicated" do
    describe "GET #index" do
      it "returns http success and rooms for the hostel of the current user" do
        login(@correct_user)
        get_with_token :index
        expect(response).to be_success
        expect(json.length).to eq(5)
      end

      it "returns http success and no rooms for a user with hostel with no rooms" do
        login(@incorrect_user)
        get_with_token :index
        expect(response).to be_success
        expect(json.length).to eq(0)
      end
    end

    describe "GET #show" do
      before do
        @room = @correct_user.hostel.rooms.sample
      end

      it "returns http success and a room" do
        login(@correct_user)
        get_with_token :show, {id: @room.id}
        expect(response).to be_success
        expect(json['id']).to eq(@room.id)
      end

      it "returns http not found when room doesn't belong to user's hostel" do
        login(@incorrect_user)
        get_with_token :show, {id: @room.id}
        expect(response).to be_not_found
      end
    end

    describe "DELETE #destroy" do
      before :each do
        @room = FactoryGirl.create(:room, hostel: @hostel_main)
        expect(Room.find(@room.id)).to_not be_nil
      end
      it "returns http success and removes a room" do
        login(@correct_user)
        delete_with_token :destroy, {id: @room.id}
        expect(response).to have_http_status(:success)
        expect{Room.find(@room.id)}.to raise_error(ActiveRecord::RecordNotFound)
      end
      it "returns http not found when room doesn't belong to user's hostel" do
        login(@incorrect_user)
        delete_with_token :destroy, {id: @room.id}
        expect(response).to be_not_found
      end
    end

    describe "POST #create" do
      before do
        login(@correct_user)
      end

      it "adds entry in DB when correct data is provided" do
        room = FactoryGirl.attributes_for(:room, hostel_id: @hostel_main)
        post_with_token :create, room
        expect(response).to be_success
        expect(Room.find_by_number(room[:number])).to_not be_nil
      end

      it "returns bad request with no data" do
        post_with_token :create, {}
        expect(response).to be_bad_request
      end
    end

    describe "PUT #update" do
      it "returns http success" do
        login(@correct_user)
        @room = @correct_user.hostel.rooms.sample
        @room.number += '666'
        put_with_token :update, @room.as_json
        expect(response).to be_success
        expect(Room.find(@room.id).number).to eq(@room.number)
      end
    end
  end
end

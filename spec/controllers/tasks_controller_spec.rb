require 'rails_helper'

RSpec.describe TasksController, type: :controller do
  include Helpers
  before do
    activate_authlogic
    @hostel_main = FactoryGirl.create(:hostel, :with_rooms)
    @hostel_second = FactoryGirl.create(:hostel)
    @correct_user = FactoryGirl.create(:user, hostel: @hostel_main)
    @incorrect_user = FactoryGirl.create(:user, hostel: @hostel_second)
    FactoryGirl.create_list(:task, 5, hostel: @hostel_main)

  end

  context "" do
    cleaner = FactoryGirl.create(:user, hostel: FactoryGirl.create(:hostel), group: 'cleaner')
    it_behaves_like "unauthorized"
    it_behaves_like "unauthorized", user: cleaner, only: [:destroy, :create, :update]
  end

  describe "while authenicated" do
    describe "GET #index" do
      it "returns http success and tasks for the hostel of the current user" do
        login(@correct_user)
        get_with_token :index
        expect(response).to be_success
        expect(json.length).to eq(@correct_user.hostel.tasks.count)
      end

      it "returns http success and no tasks for a user with hostel with no tasks" do
        login(@incorrect_user)
        get_with_token :index
        expect(response).to be_success
        expect(json.length).to eq(0)
      end
    end

    describe "GET #show" do
      before do
        @task = @correct_user.hostel.tasks.sample
      end

      it "returns http success and a task" do
        login(@correct_user)
        get_with_token :show, {id: @task.id}
        expect(response).to be_success
        expect(json['id']).to eq(@task.id)
      end

      it "returns http not found when task doesn't belong to user's hostel" do
        login(@incorrect_user)
        get_with_token :show, {id: @task.id}
        expect(response).to be_not_found
      end
    end

    describe "DELETE #destroy" do
      before :each do
        @task = FactoryGirl.create(:task, hostel: @hostel_main)
        expect(Task.find(@task.id)).to_not be_nil
      end
      it "returns http success and removes a task" do
        login(@correct_user)
        delete_with_token :destroy, {id: @task.id}
        expect(response).to have_http_status(:success)
        expect{Task.find(@task.id)}.to raise_error(ActiveRecord::RecordNotFound)
      end
      it "returns http not found when task doesn't belong to user's hostel" do
        login(@incorrect_user)
        delete_with_token :destroy, {id: @task.id}
        expect(response).to be_not_found
      end
    end

    describe "POST #create" do
      before do
        login(@correct_user)
      end

      it "adds entry in DB when correct data is provided" do
        task = FactoryGirl.attributes_for(:task, hostel_id: @hostel_main)
        post_with_token :create, task
        expect(response).to be_success
        expect(Task.find_by_name(task[:name])).to_not be_nil
      end

      it "returns bad request with no data" do
        post_with_token :create, {}
        expect(response).to be_bad_request
      end
    end

    describe "PUT #update" do
      it "returns http success" do
        login(@correct_user)
        @task = @correct_user.hostel.tasks.sample
        @task.name += 'vika'
        put_with_token :update, @task.as_json
        expect(response).to be_success
        expect(Task.find(@task.id).name).to eq(@task.name)
      end
    end
  end
end

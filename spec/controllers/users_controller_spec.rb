require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  include Helpers
  before :each do
    activate_authlogic
  end

  before :all do
    @hostel_main = FactoryGirl.create(:hostel)
    @hostel_second = FactoryGirl.create(:hostel)
    @correct_user = FactoryGirl.create(:user, hostel: @hostel_main)
    @cleaner = FactoryGirl.create(:user, hostel: @hostel_main, group: 'cleaner')
    @incorrect_user = FactoryGirl.create(:user, hostel: @hostel_second)
  end

  context "" do
    cleaner = FactoryGirl.create(:user, hostel: FactoryGirl.create(:hostel), group: 'cleaner')
    it_behaves_like "unauthorized"
    it_behaves_like "unauthorized", user: cleaner
  end

  describe "while authenicated" do
    describe "GET #index" do
      it "returns http success and users for the hostel of the current user" do
        login(@correct_user)
        get_with_token :index
        expect(response).to be_success
        expect(json.length).to eq(@correct_user.hostel.users.count)
      end

      it "returns http success and no users for a user with hostel with no users" do
        login(@incorrect_user)
        get_with_token :index
        expect(response).to be_success
        expect(json.length).to eq(1)
      end
    end

    describe "GET #show" do
      before do
        @user = @correct_user.hostel.users.sample
      end

      it "returns http success and a user" do
        login(@correct_user)
        get_with_token :show, {id: @user.id}
        expect(response).to be_success
        expect(json['id']).to eq(@user.id)
      end

      it "returns http not found when user doesn't belong to user's hostel" do
        login(@incorrect_user)
        get_with_token :show, {id: @user.id}
        expect(response).to be_not_found
      end
    end
    
    describe "DELETE #destroy" do
      before :each do
        @user = FactoryGirl.create(:user, hostel: @hostel_main)
        expect(User.find(@user.id)).to_not be_nil
      end
      it "returns http success and removes a user" do
        login(@correct_user)
        delete_with_token :destroy, {id: @user.id}
        expect(response).to have_http_status(:success)
        expect{User.find(@user.id)}.to raise_error(ActiveRecord::RecordNotFound)
      end
      it "returns http not found when user doesn't belong to user's hostel" do
        login(@incorrect_user)
        delete_with_token :destroy, {id: @user.id}
        expect(response).to be_not_found
      end
    end

    describe "POST #create" do
      before do
        login(@correct_user)
      end

      it "adds entry in DB when correct data is provided" do
        user = FactoryGirl.attributes_for(:user, hostel_id: @hostel_main)
        post_with_token :create, user
        expect(response).to be_success
        expect(User.find_by_email(user[:email])).to_not be_nil
      end

      it "returns bad request with no data" do
        post_with_token :create, {}
        expect(response).to be_bad_request
      end
    end

    describe "PUT #update" do
      it "returns http success" do
        login(@correct_user)
        @user = @correct_user.hostel.users.sample
        @user.first_name += '_vovan'
        put_with_token :update, @user.as_json
        expect(response).to be_success
        expect(User.find(@user.id).first_name).to eq(@user.first_name)
      end
    end
  end
end

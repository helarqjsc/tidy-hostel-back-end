require 'rails_helper'

RSpec.describe CurrentTasksController, type: :controller do
  include Helpers
  before do
    activate_authlogic
    @hostel_main = FactoryGirl.create(:hostel, :with_rooms)
    @hostel_second = FactoryGirl.create(:hostel)
    @correct_user = FactoryGirl.create(:user, hostel: @hostel_main)
    @incorrect_user = FactoryGirl.create(:user, hostel: @hostel_second)
    @room = @hostel_main.rooms.first
    FactoryGirl.create_list(:current_task, 5, :with_tasks, hostel: @hostel_main, room: @room)

    ['get', 'post', 'delete', 'put'].each do |act|
      self.class.send(:define_method, "#{act}_with_room_id") do |*args|
        params = args[1] || {}
        send("#{act}_with_token".to_sym, args[0], {room_id: @room.id}.merge(params))
      end
    end
  end
  

  
  context "" do
    methods = {
        all: {verb: :get, params: {} },
        index: {verb: :get, params: {} },
        show: {verb: :get, params: {id: 1} },
        destroy: {verb: :delete, params: {id: 1} },
        create: {verb: :post, params: {id: 1} },
        update: {verb: :put, params: {id: 1} }
    }
    hostel = FactoryGirl.create(:hostel, :with_rooms)
    room = hostel.rooms.first
    cleaner = FactoryGirl.create(:user, hostel: hostel, group: 'cleaner')
    it_behaves_like "unauthorized", additional_parameters: { room_id: room.id }, methods: methods
    it_behaves_like "unauthorized", user: cleaner, only: [:destroy, :create], additional_parameters: { room_id: room.id }
  end

  describe "while authenicated" do
    describe "GET #all" do
      it "returns http success and all current_tasks for the hostel of the current user" do
        login(@correct_user)
        get_with_token :all
        expect(response).to be_success
        expect(json.length).to eq(@hostel_main.current_tasks.count)
      end
    end

    describe "GET #index" do
      it "returns http success and current_tasks for the specified room" do
        login(@correct_user)
        get_with_room_id :index
        expect(response).to be_success
        expect(json.length).to eq(@room.current_tasks.count)
      end

      it "returns http not found for a user with hostel with no current_tasks" do
        login(@incorrect_user)
        get_with_room_id :index
        expect(response).to be_not_found
      end
    end

    describe "GET #show" do
      before do
        @current_task = @room.current_tasks.sample
      end

      it "returns http success and a current_task" do
        login(@correct_user)
        get_with_room_id :show, {id: @current_task.id}
        expect(response).to be_success
        expect(json['id']).to eq(@current_task.id)
      end

      it "returns http not found when current_task doesn't belong to user's hostel" do
        login(@incorrect_user)
        get_with_room_id :show, {id: @current_task.id}
        expect(response).to be_not_found
      end
    end

    describe "DELETE #destroy" do
      before :each do
        @current_task = FactoryGirl.create(:current_task, :with_tasks, hostel: @hostel_main, room: @room)
        expect(CurrentTask.find(@current_task.id)).to_not be_nil
      end

      it "returns http success and removes a current_task" do
        login(@correct_user)
        delete_with_room_id :destroy, {id: @current_task.id}
        expect(response).to have_http_status(:success)
        expect{CurrentTask.find(@current_task.id)}.to raise_error(ActiveRecord::RecordNotFound)
      end

      it "returns http not found when current_task doesn't belong to user's hostel" do
        login(@incorrect_user)
        delete_with_room_id :destroy, {id: @current_task.id}
        expect(response).to be_not_found
      end
    end

    describe "POST #create" do
      before do
        login(@correct_user)
      end

      it "adds entry in DB when correct data is provided" do
        task = FactoryGirl.create(:task, hostel: @hostel_main)
        current_task = FactoryGirl.attributes_for(:current_task, task_id: task.id)
        post_with_room_id :create, current_task
        expect(response).to be_success
        expect(CurrentTask.find_by_task_id(current_task[:task_id])).to_not be_nil
      end

      it "returns bad request with no data" do
        post_with_room_id :create, {}
        expect(response).to be_bad_request
      end
    end

    describe "PUT #update" do
      it "returns http success" do
        login(@correct_user)
        task = FactoryGirl.create(:task, hostel: @hostel_main)
        @current_task = @room.current_tasks.sample
        @current_task.task = task
        put_with_room_id :update, @current_task.as_json
        expect(response).to be_success
        expect(CurrentTask.find(@current_task.id).task_id).to eq(@current_task.task_id)
      end
    end
  end
end

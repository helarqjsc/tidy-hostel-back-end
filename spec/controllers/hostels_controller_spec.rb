require 'rails_helper'

RSpec.describe HostelsController, type: :controller do
  include Helpers
  before do
    activate_authlogic
    @hostel_main = FactoryGirl.create(:hostel, :with_rooms)
    FactoryGirl.create_list(:hostel, 3)
  end

  context "" do
    owner = FactoryGirl.create(:user, hostel: FactoryGirl.create(:hostel), group: 'owner')
    methods = {
        mine: {verb: :get, params: {} },
        index: {verb: :get, params: {} },
        show: {verb: :get, params: {id: 1} },
        destroy: {verb: :delete, params: {id: 1} },
        create: {verb: :post, params: {id: 1} },
        update: {verb: :put, params: {id: 1} }
    }
    it_behaves_like "unauthorized", methods: methods
    it_behaves_like "unauthorized", user: owner, methods: methods, except: [:mine, :update]
  end

  describe "while authenticated as a cleaner" do
    before do
      @cleaner = FactoryGirl.create(:user, hostel: @hostel_main, group: 'cleaner')
      login(@cleaner)
    end

    describe "GET #mine" do
      it "returns info about hostel that user belongs to" do
        get_with_token :mine
        expect(response).to be_success
        expect(json['name']).to eq(@hostel_main.name)
      end
    end
  end

  describe "while authenicated as root" do
    before do
      @root = FactoryGirl.create(:user, hostel: @hostel_main, group: 'root')
      login(@root)
    end

    describe "GET #index" do
      it "returns http success and hostels for the hostel of the current user" do
        get_with_token :index
        expect(response).to be_success
        expect(json.length).to eq(Hostel.all.count)
      end
    end

    describe "GET #show" do
      before do
        @hostel = Hostel.all.sample
      end

      it "returns http success and a hostel" do
        get_with_token :show, {id: @hostel.id}
        expect(response).to be_success
        expect(json['id']).to eq(@hostel.id)
      end
    end

    describe "DELETE #destroy" do
      before :each do
        @hostel = FactoryGirl.create(:hostel)
        expect(Hostel.find(@hostel.id)).to_not be_nil
      end

      it "returns http success and removes a hostel" do
        delete_with_token :destroy, {id: @hostel.id}
        expect(response).to have_http_status(:success)
        expect{Hostel.find(@hostel.id)}.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    describe "POST #create" do
      it "adds entry in DB when correct data is provided" do
        hostel = FactoryGirl.attributes_for(:hostel)
        post_with_token :create, hostel
        expect(response).to be_success
        expect(Hostel.find_by_name(hostel[:name])).to_not be_nil
      end

      it "returns bad request with no data" do
        post_with_token :create, {}
        expect(response).to be_bad_request
      end
    end

    describe "PUT #update" do
      it "returns http success" do
        @hostel = Hostel.all.sample
        @hostel.name += 'nyashka'
        put_with_token :update, @hostel.as_json
        expect(response).to be_success
        expect(Hostel.find(@hostel.id).name).to eq(@hostel.name)
      end
    end
  end
end

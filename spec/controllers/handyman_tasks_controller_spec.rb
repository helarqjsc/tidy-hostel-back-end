require 'rails_helper'
require "base64"

RSpec.describe HandymanTasksController, type: :controller do
  include Helpers
  before do
    activate_authlogic
    @hostel_main = FactoryGirl.create(:hostel, :with_rooms)
    @hostel_second = FactoryGirl.create(:hostel)
    @correct_user = FactoryGirl.create(:user, hostel: @hostel_main)
    @incorrect_user = FactoryGirl.create(:user, hostel: @hostel_second)
    @room = @hostel_main.rooms.first
    FactoryGirl.create_list(:handyman_task, 5, room: @room, user: @correct_user)

  end
  

  
  context "" do
    hostel = FactoryGirl.create(:hostel, :with_rooms)
    room = hostel.rooms.first
    cleaner = FactoryGirl.create(:user, hostel: hostel, group: 'cleaner')
    it_behaves_like "unauthorized", additional_parameters: { room_id: room.id }
    it_behaves_like "unauthorized", user: cleaner, only: [:destroy, :create], additional_parameters: { room_id: room.id }
  end

  describe "while authenicated" do

    describe "GET #index" do
      it "returns http success and handyman_tasks for the specified room" do
        login(@correct_user)
        get_with_token :index
        expect(response).to be_success
        expect(json.length).to eq(@room.handyman_tasks.count)
      end

      it "returns http not found for a user with hostel with no handyman_tasks" do
        login(@incorrect_user)
        get_with_token :index
        expect(response).to be_success
        expect(json.length).to be_zero
      end
    end

    describe "GET #show" do
      before do
        @handyman_task = @room.handyman_tasks.sample
      end

      it "returns http success and a handyman_task" do
        login(@correct_user)
        get_with_token :show, {id: @handyman_task.id}
        expect(response).to be_success
        expect(json['id']).to eq(@handyman_task.id)
      end

      it "returns http not found when handyman_task doesn't belong to user's hostel" do
        login(@incorrect_user)
        get_with_token :show, {id: @handyman_task.id}
        expect(response).to be_not_found
      end
    end

    describe "DELETE #destroy" do
      before :each do
        @handyman_task = FactoryGirl.create(:handyman_task, user: @correct_user, room: @room)
        expect(HandymanTask.find(@handyman_task.id)).to_not be_nil
      end

      it "returns http success and removes a handyman_task" do
        login(@correct_user)
        delete_with_token :destroy, {id: @handyman_task.id}
        expect(response).to have_http_status(:success)
        expect{HandymanTask.find(@handyman_task.id)}.to raise_error(ActiveRecord::RecordNotFound)
      end

      it "returns http not found when handyman_task doesn't belong to user's hostel" do
        login(@incorrect_user)
        delete_with_token :destroy, {id: @handyman_task.id}
        expect(response).to be_not_found
      end
    end

    describe "POST #create" do
      before do
        login(@correct_user)
      end

      it "adds entry in DB when correct data is provided" do
        handyman_task = FactoryGirl.attributes_for(:handyman_task, user_id: @correct_user.id, room_id: @room.id)
        post_with_token :create, handyman_task
        expect(response).to be_success
        expect(HandymanTask.find_by_message(handyman_task[:message])).to_not be_nil
      end

      it "returns bad request with no data" do
        post_with_token :create, {}
        expect(response).to be_bad_request
      end
    end

    describe "PUT #update" do
      it "returns http success" do
        login(@correct_user)
        @handyman_task = @room.handyman_tasks.sample
        @handyman_task.message += "lel"
        put_with_token :update, @handyman_task.as_json
        expect(response).to be_success
        expect(HandymanTask.find(@handyman_task.id).message).to eq(@handyman_task.message)
      end
    end
  end
end

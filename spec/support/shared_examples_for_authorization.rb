RSpec.shared_examples "unauthorized" do |args|
  args ||= {}

  methods = args[:methods] || {
      index: {verb: :get, params: {} },
      show: {verb: :get, params: {id: 1} },
      destroy: {verb: :delete, params: {id: 1} },
      create: {verb: :post, params: {id: 1} },
      update: {verb: :put, params: {id: 1} }
  }
  args[:only] ||= methods.keys
  args[:except] ||= []
  args[:only] -= args[:except]
  args[:additional_parameters] ||= {}
  user = args[:user]
  description = "while unauthenticated"
  description = "while authenticated as #{user.group}" if user

  describe description do
    args[:only].each do |method|
      m = methods[method]
      it "#{m[:verb].to_s.upcase} \##{method} returns http unauthorized" do
        activate_authlogic
        login(user) if user
        send(m[:verb], method, m[:params].merge(args[:additional_parameters]))
        expect_unauth
      end
    end
  end

  def expect_unauth
    expect(response).to be_unauthorized
  end
end
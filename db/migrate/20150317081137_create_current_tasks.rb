class CreateCurrentTasks < ActiveRecord::Migration
  def change
    create_table :current_tasks do |t|
      t.references :task
      t.references :room
      t.boolean :done, default: false

      t.timestamps null: false
    end
    add_foreign_key :current_tasks, :tasks
    add_foreign_key :current_tasks, :rooms

  end
end

class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.references :hostel, index: true
      t.string :number
      t.integer :room_class
      t.integer :status
      t.datetime :last_cleaned_at
      t.integer :last_cleaner_id
      t.integer :cleaner_id

      t.timestamps null: false
    end
    add_foreign_key :rooms, :hostels
  end
end

class AddAttachmentPhotoToHandymanTasks < ActiveRecord::Migration
  def self.up
    change_table :handyman_tasks do |t|
      t.attachment :photo
    end
  end

  def self.down
    remove_attachment :handyman_tasks, :photo
  end
end

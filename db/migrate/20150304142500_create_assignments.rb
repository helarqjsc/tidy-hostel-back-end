class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.date :date
      t.references :hostel, index: true
      t.references :room, index: true
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :assignments, :hostels
    add_foreign_key :assignments, :rooms
    add_foreign_key :assignments, :users
  end
end

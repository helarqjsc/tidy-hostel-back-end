class CreateHostels < ActiveRecord::Migration
  def change
    create_table :hostels do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :website

      t.string :country
      t.string :city
      t.string :postal_code
      t.string :address_1
      t.string :address_2

      t.string :facebook
      t.string :vkontakte
      t.string :twitter
      t.string :foursquare

      t.string :cover_photo_url
      t.string :logo_url

      #todo: add image attachments

      t.timestamps null: false
    end
  end
end

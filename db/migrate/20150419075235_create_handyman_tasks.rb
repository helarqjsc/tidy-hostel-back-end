class CreateHandymanTasks < ActiveRecord::Migration
  def change
    create_table :handyman_tasks do |t|
      t.references :room, index: true
      t.references :user, index: true
      t.text :message
      t.integer :status
      t.integer :priority

      t.timestamps null: false
    end
    add_foreign_key :handyman_tasks, :rooms
    add_foreign_key :handyman_tasks, :users
  end
end

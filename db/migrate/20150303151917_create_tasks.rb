class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :hostel, index: true
      t.string :name
      t.boolean :enabled, default: false

      t.timestamps null: false
    end
    add_foreign_key :tasks, :hostels
  end
end

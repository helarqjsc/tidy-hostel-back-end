# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150419081642) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assignments", force: :cascade do |t|
    t.date     "date"
    t.integer  "hostel_id"
    t.integer  "room_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "assignments", ["hostel_id"], name: "index_assignments_on_hostel_id", using: :btree
  add_index "assignments", ["room_id"], name: "index_assignments_on_room_id", using: :btree
  add_index "assignments", ["user_id"], name: "index_assignments_on_user_id", using: :btree

  create_table "current_tasks", force: :cascade do |t|
    t.integer  "task_id"
    t.integer  "room_id"
    t.boolean  "done",       default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "handyman_tasks", force: :cascade do |t|
    t.integer  "room_id"
    t.integer  "user_id"
    t.text     "message"
    t.integer  "status"
    t.integer  "priority"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  add_index "handyman_tasks", ["room_id"], name: "index_handyman_tasks_on_room_id", using: :btree
  add_index "handyman_tasks", ["user_id"], name: "index_handyman_tasks_on_user_id", using: :btree

  create_table "hostels", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "website"
    t.string   "country"
    t.string   "city"
    t.string   "postal_code"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "facebook"
    t.string   "vkontakte"
    t.string   "twitter"
    t.string   "foursquare"
    t.string   "cover_photo_url"
    t.string   "logo_url"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "rooms", force: :cascade do |t|
    t.integer  "hostel_id"
    t.string   "number"
    t.integer  "room_class"
    t.integer  "status"
    t.datetime "last_cleaned_at"
    t.integer  "last_cleaner_id"
    t.integer  "cleaner_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "rooms", ["hostel_id"], name: "index_rooms_on_hostel_id", using: :btree

  create_table "tasks", force: :cascade do |t|
    t.integer  "hostel_id"
    t.string   "name"
    t.boolean  "enabled",    default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "tasks", ["hostel_id"], name: "index_tasks_on_hostel_id", using: :btree

  create_table "user_sessions", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "photo_url"
    t.integer  "group"
    t.integer  "hostel_id"
    t.string   "email",               null: false
    t.string   "crypted_password",    null: false
    t.string   "password_salt",       null: false
    t.string   "persistence_token",   null: false
    t.string   "single_access_token", null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "users", ["hostel_id"], name: "index_users_on_hostel_id", using: :btree

  add_foreign_key "assignments", "hostels"
  add_foreign_key "assignments", "rooms"
  add_foreign_key "assignments", "users"
  add_foreign_key "current_tasks", "rooms"
  add_foreign_key "current_tasks", "tasks"
  add_foreign_key "handyman_tasks", "rooms"
  add_foreign_key "handyman_tasks", "users"
  add_foreign_key "rooms", "hostels"
  add_foreign_key "tasks", "hostels"
  add_foreign_key "users", "hostels"
end

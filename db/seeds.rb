# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

booleans = [true, false]

@hostel = Hostel.create(
    name: 'Super Hostel',
    phone: '+79650013392',
    email: 'hi@superhostel.com',
    website: 'http://www.superhostel.com',
    country: 'USA',
    city: 'New York',
    postal_code: '123456',
    address_1: '1st Line',
    address_2: '37',
    facebook: 'http://facebook.com/superhostel',
    vkontakte: 'http://vk.com/superhostel',
    twitter: 'http://twitter.com/superhostel',
    foursquare: 'http://foursquare.com/superhostel',
    cover_photo_url: 'http://upload.wikimedia.org/wikipedia/commons/3/3a/Hippolyte_Sebron_-_Rue_De_New-York_En_1840.jpg',
    logo_url: 'https://s-media-cache-ak0.pinimg.com/originals/9f/5a/ec/9f5aec50bc3b3493fc4ac6377ff6a167.jpg'
)

owner = User.create(
    hostel: @hostel,
    password: '1234',
    group: 'owner',
    email: 'hello@andreykeske.com',
    phone: '+79650013392',
    first_name: 'Andrey',
    last_name: 'Keske',
    photo_url: 'http://cs614623.vk.me/v614623573/1b887/13qRrLUmdTg.jpg'
)
cleaner = User.create(
    hostel: @hostel,
    password: '1234',
    group: 'cleaner',
    email: 'bruce@lee.com',
    phone: '+71234567890',
    first_name: 'Bruce',
    last_name: 'Lee',
    photo_url: 'http://www.impactonline.co/images/articles/people/1Bruce-Lee2.jpg'
)
admin = User.create(
    hostel: @hostel,
    email: 'bender@whores.com',
    password: '1234',
    group: 'admin',
    phone: '+71234567890',
    first_name: 'Bender',
    last_name: 'Futurama',
    photo_url: 'http://cdn.pastemagazine.com/www/blogs/lists/2011/12/04/bender.jpg'
)

(1..5).each do |n|
  r = Room.new
  r.number = n
  r.hostel = @hostel
  r.room_class = rand(Room.room_classes.length)
  r.status = rand(Room.statuses.length)
  r.cleaner_id = User.all.sample.id
  r.save
end

['chandelier',
         'dust',
         'sill',
         'glassAndMirrors',
         'bed',
         'carpet',
         'floor',
         'washFloor',
         'checkEquipment',
         'garbage'].each do |name|
  t = Task.new
  t.name = name
  t.hostel = @hostel
  t.enabled = booleans.sample
  t.save
end

@hostel.rooms.each do |room|
  @hostel.tasks.each do |task|
    CurrentTask.create({task: task, room: room, done: booleans.sample})
  end
end

[
  'Сломалась ручка, какой раз я уже прошу сделать это нормально, разве это так сложно? Давай-те стараться вместе, что бы гостям было комфортно тут оставаться!',
  'Унитаз не смывает',
  'Там что-то кран течет!',
  'Полка сломалась',
  'Там что-то кран течет!',
  'Полка сломалась'
].each do |message|
  t = HandymanTask.new
  t.message = message
  t.room = @hostel.rooms.sample
  t.user = @hostel.users.sample
  t.status = rand(HandymanTask.statuses.length)
  t.priority = rand(HandymanTask.priorities.length)
  t.save!
end


# (1..10).each do |n|
#   a = Assignment.new
#   a.hostel = hostel
#   a.user = cleaner
#   a.room = Room.find(n)
#   a.date = Date.today + n
#   a.save
# end
class UsersController < ApplicationController
  # before_filter :render_nothing, only: [:destroy, :create, :update]
  before_action { require_user [:root, :admin, :owner] }
  before_action :find_user, only: [:show, :destroy, :update]

  def index
    #todo: make it possible for an admin to retrieve rooms for any hostel
    render json: current_user.hostel.users
  end

  def show
    render json: @user
  end

  def current
    render json: current_user
  end

  def destroy
    @user.destroy
    render_nothing
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render_nothing
    else
      render_bad_request
    end
  end

  def update
    @user.update_attributes(user_params)
    if @user.save
      render_nothing
    else
      render_bad_request
    end
  end

  private
  def find_user
    @user = User.find(params[:id])
    unless current_user.root? or @user.hostel == current_user.hostel
      render_not_found
      return false
    end
  end

  def user_params
    p = params.permit(:email, :password, :group, :hostel_id, :first_name, :last_name, :phone, :photo_url)
    #todo: creating tasks for other hostels should be forbidden
    p[:hostel_id] ||= current_user.hostel.id
    p
  end
end

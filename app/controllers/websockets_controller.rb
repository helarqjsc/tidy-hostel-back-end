class WebsocketsController < WebsocketRails::BaseController

  def authorize_channels
    #channel names look like "hostel_id1.users"
    hostel_id = message[:channel].scan(/hostel_id(\d+)\./)[0][0].to_i
    user = User.find_by_single_access_token(message[:token])

    p "user with id #{user.id} is trying to subscribe to channel #{message[:channel]}:"
    if user and user.hostel.id == hostel_id
      accept_channel
    else
      deny_channel
    end
  end

end
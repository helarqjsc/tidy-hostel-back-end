class RoomsController < ApplicationController
  before_action lambda{ require_user [:root, :admin, :owner, :cleaner] }, {only: [:index, :show]}
  before_action lambda{ require_user [:root, :admin, :owner] }, {only: [:destroy, :create, :update]}

  before_action :find_room, only: [:show, :destroy, :update]

  def index
    #todo: make it possible for an admin to retrieve rooms for any hostel
    render json: current_user.hostel.rooms
  end

  def show
    render json: @room
  end

  def destroy
    @room.destroy
    render_nothing
  end

  def create
    @room = Room.new(room_params)
    if @room.save
      render_nothing
    else
      render_bad_request
    end
  end

  def update
    @room.update_attributes(room_params)
    if @room.save
      render_nothing
    else
      render_bad_request
    end
  end

  private
  def find_room
    @room = Room.find(params[:id])
    unless current_user.root? or @room.hostel == current_user.hostel
      render_not_found
      return false
    end
  end

  def room_params
    p = params.permit(:number, :hostel_id, :room_class, :status, :last_cleaner_id, :last_cleaned_at)
    #todo: creating tasks for other hostels should be forbidden
    p[:hostel_id] ||= current_user.hostel.id
    p
  end
end

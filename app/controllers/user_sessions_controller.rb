class UserSessionsController < ApplicationController

  def create
    @user_session = UserSession.new(session_params)
    if @user_session.save
      #todo: enable token reset on login in production
      # @user_session.user.reset_single_access_token!
      render json: {status: :success, token: @user_session.user.single_access_token}
    else
      render json: {status: :fail}, status: :unauthorized
    end
  end

  def destroy
    @user_session = UserSession.find
    # @user_session.user.reset_single_access_token!
    @user_session.destroy
    render json: {status: :success}
  end

  private
  def session_params
    params.permit(:email, :password)
  end
end

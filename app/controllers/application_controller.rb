class ApplicationController < ActionController::Base
  protect_from_forgery # See ActionController::RequestForgeryProtection for details
  helper_method :current_user_session, :current_user

  rescue_from ActiveRecord::RecordNotFound do
    render_not_found
  end

  private

  #groups is an array of symbols representing allowed user groups
  def current_user
    token = params[:token]
    return @current_user if defined?(@current_user)
    return if token.nil?
    @current_user = User.find_by_single_access_token(token)
    UserSession.create(@current_user) if(@current_user.present?)
    @current_user
  end

  def require_user(groups = User.groups)
    unless current_user and groups.include?(current_user.group.to_sym) #todo: remove .to_sym somehow
      render_unauthorized
      return false
    end
  end

  def websocket_broadcast(hostel, channel, event, object)
    channel = "hostel_id#{hostel.id.to_s}.#{channel}"
    if object.is_a?(Array)
      data = object
    else
      data = [object]
    end
    WebsocketRails[channel].make_private
    WebsocketRails[channel].trigger event, data
  end

  [:success, :unauthorized, :not_found, :bad_request].each do |status|
    define_method("render_#{status.to_s}"){ render nothing: true, status: status }
  end

  def render_nothing
    render :nothing => true
  end

end

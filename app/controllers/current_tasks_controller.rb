class CurrentTasksController < ApplicationController
  before_action lambda{ require_user [:root, :admin, :owner, :cleaner] }, {only: [:all, :index, :show, :update]}
  before_action lambda{ require_user [:root, :admin, :owner] }, {only: [:destroy, :create]}

  before_action :find_current_task, only: [:show, :destroy, :update]
  before_action :find_room, only: [:index]

  def all
    render json: current_user.hostel.current_tasks
  end

  def index
    render json: @room.enabled_current_tasks.as_json
  end

  def show
    render json: @current_task
  end

  def destroy
    @current_task.destroy
    render_nothing
  end

  def create
    @current_task = CurrentTask.new(current_task_params)
    if @current_task.save
      render_nothing
    else
      render_bad_request
    end
  end

  def update
    @current_task.update_attributes(current_task_params)
    if @current_task.save
      websocket_broadcast(current_user.hostel, 'current_tasks', 'update', @current_task)
      websocket_broadcast(current_user.hostel, 'rooms', 'update', @current_task.room)
      render_nothing
    else
      render_bad_request
    end
  end

  private
  def find_current_task
    @current_task = CurrentTask.find(params[:id])
    unless current_user.root? or @current_task.room.hostel == current_user.hostel
      render_not_found
      return false
    end
  end

  def find_room
    @room = Room.find(params[:room_id])
    unless current_user.root? or @room.hostel == current_user.hostel
      render_not_found
      return false
    end
  end

  def current_task_params
    params.permit(:room_id, :task_id, :done)
    #todo: check that room id is within the current hostel
  end
end

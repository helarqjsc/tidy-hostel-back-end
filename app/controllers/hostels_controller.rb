class HostelsController < ApplicationController
  before_action lambda{ require_user [:root] }, { except: [:update, :mine] }
  before_action lambda{ require_user [:root, :admin, :owner] }, { only: :update }
  before_action lambda{ require_user [:root, :admin, :owner, :cleaner] }, { only: :mine }
  before_action :find_hostel, only: [:show, :destroy, :update]

  def index
    render json: Hostel.all
  end

  def mine
    render json: current_user.hostel.as_json
  end

  def show
    render json: @hostel
  end

  def destroy
    @hostel.destroy
    render_nothing
  end

  def create
    @hostel = Hostel.new(hostel_params)
    if @hostel.save
      render_nothing
    else
      render_bad_request
    end
  end

  def update
    @hostel.update_attributes(hostel_params)
    if @hostel.save
      render_nothing
    else
      render_bad_request
    end
  end

  private
  def find_hostel
    @hostel = Hostel.find(params[:id])
  end

  def hostel_params
    params.permit(:name,
                  :phone,
                  :email,
                  :website,
                  :country,
                  :city,
                  :postal_code,
                  :address_1,
                  :address_2,
                  :facebook,
                  :vkontakte,
                  :twitter,
                  :foursquare)
  end
end

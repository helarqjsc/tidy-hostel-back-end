require "base64"

class HandymanTasksController < ApplicationController
  before_action lambda{ require_user [:root, :admin, :owner, :cleaner] }, {only: [:index, :show, :update]}
  before_action lambda{ require_user [:root, :admin, :owner] }, {only: [:destroy, :create]}

  before_action :find_handyman_task, only: [:show, :destroy, :update]
  before_action :handyman_task_params, only: [:create, :update]
  # before_action :decode_image, only: [:create, :update]


  def index
    render json: current_user.hostel.handyman_tasks
  end

  def show
    render json: @handyman_task
  end

  def destroy
    @handyman_task.destroy
    render_nothing
  end

  def create
    @handyman_task = HandymanTask.new(handyman_task_params)
    if @handyman_task.save
      websocket_broadcast(current_user.hostel, 'handyman_tasks', 'create', @handyman_task)
      render_nothing
    else
      render_bad_request
    end
  end

  def update
    @handyman_task.update_attributes(handyman_task_params)
    if @handyman_task.save
      websocket_broadcast(current_user.hostel, 'handyman_tasks', 'update', @handyman_task)
      render_nothing
    else
      render_bad_request
    end
  end

  private
  def find_handyman_task
    @handyman_task = HandymanTask.find(params[:id])
    unless current_user.root? or @handyman_task.room.hostel == current_user.hostel
      render_not_found
      return false
    end
  end

  def handyman_task_params
    params.permit(:room_id, :user_id, :message, :status, :priority, :photo_data)
  end

end

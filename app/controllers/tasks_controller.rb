class TasksController < ApplicationController
  before_action lambda{ require_user [:root, :admin, :owner, :cleaner] }, {only: [:index, :show]}
  before_action lambda{ require_user [:root, :admin, :owner] }, {only: [:destroy, :create, :update]}

  before_action :find_task, only: [:show, :destroy, :update]

  def index
    #todo: make it possible for an admin to retrieve tasks for any hostel
    render json: current_user.hostel.tasks
  end

  def show
    render json: @task
  end

  def destroy
    @task.destroy
    render_nothing
  end

  def create
    @task = Task.new(task_params)
    if @task.save
      render_nothing
    else
      render_bad_request
    end
  end

  def update
    @task.update_attributes(task_params)
    if @task.save
      websocket_broadcast(current_user.hostel, 'tasks', 'update', @task)
      websocket_broadcast(current_user.hostel, 'current_tasks', 'update', @task.hostel.current_tasks)
      websocket_broadcast(current_user.hostel, 'rooms', 'update', @task.hostel.rooms)
      render_nothing
    else
      render_bad_request
    end
  end

  private
  def find_task
    @task = Task.find(params[:id])
    unless current_user.root? or @task.hostel == current_user.hostel
      render_not_found
      return false
    end
  end

  def task_params
    p = params.permit(:name, :enabled, :hostel_id)
    #todo: creating tasks for other hostels should be forbidden
    p[:hostel_id] ||= current_user.hostel.id
    p
  end
end

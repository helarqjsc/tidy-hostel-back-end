class Room < ActiveRecord::Base
  belongs_to :hostel
  has_many :current_tasks
  has_many :handyman_tasks

  validates_presence_of :hostel, :number
  enum status:  [:clean, :dirty]#, :in_process]
  enum room_class:  [:economic, :business, :vip]

  def enabled_current_tasks
    CurrentTask.where(room: self).joins(:task).merge(Task.where(enabled: true))
  end

  def update_status!
    self.status = 'clean'
    #if all tasks are completed, the room is marked as clean, otherwise dirty
    enabled_current_tasks.each do |task|
      self.status = 'dirty' unless task.done
    end
    save!
  end
end

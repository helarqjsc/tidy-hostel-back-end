class HandymanTask < ActiveRecord::Base
  belongs_to :room
  belongs_to :user

  enum status:  [:open, :during, :closed]
  enum priority:  [:high, :normal, :low]
  attr_accessor :photo_data

  paperclip_opts = {
      storage: :filesystem,
      path: ":rails_root/public/system/:class/:attachment/:id_partition/:style/:filename"
  }

  if Rails.env.production?
    paperclip_opts.merge!({
      :storage => :s3,
      :s3_credentials => {
        :bucket => ENV['S3_BUCKET_NAME'],
        :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
        :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
      },
      :path => ':attachment/:id/:style.:extension'
    })

  end

  has_attached_file :photo, paperclip_opts

  before_validation :set_image

  validates_presence_of [:room]
  validates_attachment_content_type :photo, content_type: ["image/jpg", "image/jpeg"]


  def as_json(options = nil)
    json = super(only: [:id, :room_id, :user_id, :message, :priority, :status, :created_at])
    json[:photo_url] = photo.url
    json
  end

  def set_image
    return if photo_updated_at or not photo_data
    StringIO.open(Base64.decode64(photo_data)) do |data|
      data.class.class_eval { attr_accessor :original_filename, :content_type }
      data.original_filename = "photo.jpg"
      data.content_type = "image/jpeg"
      self.photo = data
    end
  end
end

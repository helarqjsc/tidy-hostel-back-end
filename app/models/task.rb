class Task < ActiveRecord::Base
  belongs_to :hostel
  validates_presence_of [:hostel, :name]
  after_save :change_rooms_statuses

  private
  def change_rooms_statuses
    hostel.rooms.each do |room|
      room.update_status!
      unless self.enabled
        room.current_tasks.where(task: self).update_all(done: false)
      end
    end
  end
end

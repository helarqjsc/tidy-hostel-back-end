class CurrentTask < ActiveRecord::Base
  belongs_to :room
  belongs_to :task

  after_save :change_room_status
  validates_presence_of [:room, :task]

  #todo: add tests for this model

  def change_room_status
    room.update_status!
  end

  def as_json(options = nil)
    json = super
    json['name'] = task.name
    json['enabled'] = task.enabled
    json
  end
end

class Hostel < ActiveRecord::Base
  has_many :users
  has_many :rooms
  has_many :tasks
  has_many :assignments

  validates_uniqueness_of :name
  validates_presence_of [:name, :phone, :email]

  def current_tasks
    CurrentTask.joins(:room).where(rooms: {hostel_id: id})
  end

  def handyman_tasks
    HandymanTask.joins(:room).where(rooms: {hostel_id: id})
  end


end

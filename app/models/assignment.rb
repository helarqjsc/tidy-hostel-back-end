class Assignment < ActiveRecord::Base
  belongs_to :hostel
  belongs_to :room
  belongs_to :user
end

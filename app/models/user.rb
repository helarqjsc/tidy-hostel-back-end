class User < ActiveRecord::Base
  acts_as_authentic do |c|
    c.require_password_confirmation = false
  end

  has_many :assignments
  belongs_to :hostel

  enum group:  [:cleaner, :admin, :owner, :root]
  validates_presence_of :hostel

  def as_json(options = nil)
    json = super(only: [:id, :email, :group, :hostel_id, :first_name, :last_name, :phone, :photo_url])
    json
  end
end
